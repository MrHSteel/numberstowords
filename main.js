ones = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten']
tens = ['eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']
hundreds = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']


for (let i = 1; i <= 1000; i++) {
    let digits = String(i);
    
    if (i <= 10) {
        document.write(i + "--" + ones[i] + '<br> ');
    } else if (i < 20) {
        document.write(i + "--" + tens[i - 11] + '<br>');
    } else if (i >= 20 && i < 100) {
        let ten = Number(digits[0]);
        let one = Number(digits[1]);
        document.write(i + "--" + hundreds[ten] + " " + ones[one] + '<br> ');
    }
    else if (i >= 100 && i < 1000) {
        let hundred = Number(digits[0]);
        let ten = Number(digits[1]);
        let one = Number(digits[2]);
        
        if (ten==0 && one==0){
            document.write(i + "--" + ones[hundred] + " hundred" + '<br>');
        }
        else if (ten==1 && one>0){
            document.write(i + "--" + ones[hundred] + " hundred" + " " + "and" +" "+ tens[one-1] + '<br>');
        }else if (ten==1 && one==0){
            document.write(i + "--" + ones[hundred] + " hundred" + " " + "and" +" "+ ones[one+10] + '<br>');
        }
        else{

        document.write(i + "--" + ones[hundred] + " hundred" + " " + "and" +" "+ hundreds[ten] + " " + ones[one] + '<br>');
        }

    }
    else if (i == 1000) {
        document.write(i + "--One thousand")
    }
}